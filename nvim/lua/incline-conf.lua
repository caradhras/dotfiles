local SymbolKind = {
    [1] = 'File',
    [2] = 'Module',
    [3] = 'Namespace',
    [4] = 'Package',
    [5] = 'Class',
    [6] = 'Method',
    [7] = 'Property',
    [8] = 'Field',
    [9] = 'Constructor',
    [10] = 'Enum',
    [11] = 'Interface',
    [12] = 'Function',
    [13] = 'Variable',
    [14] = 'Constant',
    [15] = 'String',
    [16] = 'Number',
    [17] = 'Boolean',
    [18] = 'Array',
    [19] = 'Object',
    [20] = 'Key',
    [21] = 'Null',
    [22] = 'EnumMember',
    [23] = 'Struct',
    [24] = 'Event',
    [25] = 'Operator',
    [26] = 'TypeParameter',
}
local function get_sym(buf, callback)
    vim.lsp.buf_request_all(buf, 'textDocument/documentSymbol',
        { textDocument = vim.lsp.util.make_text_document_params(buf) },
        function(err, res)
            if err then
                return
            end

            local items = {}
            local traverse

            traverse = function(nodes, level)
                level = level or 0

                for _, node in ipairs(nodes) do
                    local kind_name = SymbolKind[node.kind]
                    if vim.tbl_contains({ 'Module', 'Namespace', 'Class', 'Interface', 'Method', 'Function' }, kind_name) then
                        table.insert(items, {
                            name = node.name,
                            kind = kind_name,
                            start = node.start,
                            e = node['end']
                        })
                    end
                    traverse(node.children or {}, level + 1)
                end
            end
            traverse(res or {})
            callback(items)
        end)
end
local function is_in_range(entry)
    local r, c = unpack(vim.api.nvim_win_get_cursor(0))
    r = r - 1;
    local start = entry.start
    local e = entry.e
    if (start.line < r and e.line > r) then
        return true
    elseif (start.line == r and e.line == r) then
        if (start.character <= c and e.character > c) then
            return true
        end
    elseif (start.line == r) then
        if (start.character <= c) then
            return true
        end
    elseif (e.line == r) then
        if (e.character > c) then
            return true
        end
    end
    return false
end









require("incline").setup({
    window = { margin = { vertical = 0, horizontal = 1 } },
    render = function(props)
        local filename = vim.fn.fnamemodify(vim.api.nvim_buf_get_name(props.buf), ":t")
        local icon, color = require("nvim-web-devicons").get_icon_color(filename)
        return { { icon, guifg = color }, { " " }, { filename } }
    end,
})
